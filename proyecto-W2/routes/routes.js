var express = require('express');
const router = express.Router();
const Video = require('../controllers/video_controller.js');

//STUDENT
router.post('/video', Video .create);
router.get('/video', Video .getVideo);
router.patch('/video/:id', Video .update);
router.delete('/video/:id', Video .delete);

module.exports = router;