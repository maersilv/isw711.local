const Video = require('../models/video_model.js');

//crea un nuevo video y lo agrega a la base de datos

exports.create = (req, res) => {
    var video = new video();
    video.name = req.body.name;
    video.url = req.body.url;

    video.save(function (err){
        if(err ){
            res.send(err);
        }
        res.status(201);
        res.header({
            'location': `http://localhost/video/${video.id}`
        });
         res.json(video);
    });
    
};




//obtiene los datos solicitados
exports.getVideo = (req, res) => {
    Video.find(function (err, video) {
    if (err){
        handleError(res, err ,500);
    }
     res.json(video);
});
};

//actualizar videos por identificado por el ID
exports.update = (re, res) => {
    const id = req.params.id;
    var update = req.body;
    Video.findByIdAndUpdate(id, update, {new: true},(err, video)=> {
        if(err) return res.status(500).send({message: 'Error internal server'});
       
        if(video){
            return res.status(200).send({
                video
            });
        }else{
            return res.status(404).send({
                message: 'Not found video'
            });
        }
  

    });

};

//Eliminar videos identificando por el ID

exports.delete = (req, res) =>{
    const id = req.params.id;

    Video.findByIdAndRemove(id,(err, video) => {
        if(err){
            return res.status(500).send({message: 'Error internal server'});
        }
        if(video){
            return res.status(200).send({video});

        }else{
            return res.status(404).send({
                message: 'Not found student'
            });
        }
    });
}
