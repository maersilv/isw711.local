const Student = require('../models/student_model.js');


//Crea un nuevo estudiante y lo agrega en la base de datos
exports.create = (req, res) => {
  var student = new Student();
  student.name = req.body.name;
  student.lastName = req.body.lastName;
  student.email = req.body.email;
  student.address = req.body.address;
  student.save(function (err) {
    if (err) {
      res.send(err);
    }
    res.status(201);
    res.header({
      'location': `http://localhost/student/${student._id}`
    });
    res.json(student);
  });
};

//Obtiene los datos
exports.getStudents = (req, res) => {
  Student.find(function (err, student) {
    if (err) {
      handleError(res, err, 500);
    }
    res.json(student);
  });
};


//Update a students identified by id
exports.update = (req, res) => {
  const id = req.params.id;
  var update = req.body;

  Student.findByIdAndUpdate(id, update, { new: true }, (err, student) => {
    if (err) return res.status(500).send({ message: 'Error internal server' });

    if (student) {
      return res.status(200).send({
        student
      });
    } else {
      return res.status(404).send({
        message: 'Not found student'
      });
    }
  });
};
//delete a Students identifiend by ID 
exports.delete = (req, res) => {
  const id = req.params.id;
  Student.findByIdAndRemove(id, (err, student) => {
    if (err) {
      return res.status(500).send({ message: 'Error internal server' });
    }
    if (student) {
      return res.status(200).send({ student });
    } else {
      return res.status(404).send({
        message: 'Not found student'
      });
    }
  });
}