
const mongoose = require('mongoose');
const schema =  mongoose.Schema;

const student = new schema({
	name: {type: String},
	lastName: {type: String},
	email:{type:String},
	address: {type: String},
});
module.exports = mongoose.model('student',student);