var express = require('express');
var app = express();
var cors = require('cors');

const basicAuth = require('express-basic-auth')
const {
  base64decode
} = require('nodejs-base64');

//agrega la conexion
const database = require('./config/database.js');
//agrega el driver de mongo
const mongoose = require('mongoose');
//agrega el parseardor
let bodyParser = require('body-parser');
//agrega cors para peticiones
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

//conecta la base de datos
mongoose.connect(database.url)
.then(() => {
    console.log("Successfully connected to the database");
}).catch(err => {
    console.log('Could not connect to the database. Exiting now...');
    process.exit();
});

//JWT Authentication
app.use(function (req, res, next) {
  if (req.headers["authorization"]) {
    const authToken = req.headers['authorization'].split(' ')[1];
    try {
      jwt.verify(authToken, theSecretKey, (err, decodedToken) => {
        if (err || !decodedToken) {
          res.status(401);
          res.json({
            error: "Unauthorized "
          });
        }
        console.log('Welcome', decodedToken.name);
        next();
         if (decodedToken.userId == 123) {
          next();
         }
      });
    } catch (e) {
      next();
    }

  } else {
    res.status(401);
    res.send({
      error: "Unauthorized "
    });
  }
});
// custom basic authentication
app.use(function (req, res, next) {
    if (req.headers["authorization"]) {
      const authBase64 = req.headers['authorization'].split(' ');
      const userPass = base64decode(authBase64[1]);
      const user = userPass.split(':')[0];
      const password = userPass.split(':')[1];
  
      //
      if (user === 'admin' && password == '1234') {
        next();
        return;
      }
    }
    res.status(401);
    res.send({
      error: "Unauthorized "
    });
  });
  
  //using basic auth
  app.use(basicAuth({
    users: {
      'admin': '1234',
     
    }
  }));
  


const routes = require('./routes/routes.js');
app.use(routes);

app.listen(3000, function(){
	console.log('Escuchando por el puerto 3000');
});