var express = require('express');
const router = express.Router();
const Student = require('../controllers/student_controller.js');

//STUDENT
router.post('/student', Student.create);
router.get('/students', Student.getStudents);
router.patch('/student/:id', Student.update);
router.delete('/student/:id', Student.delete);

module.exports = router;