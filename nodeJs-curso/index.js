var express = require('express');
var app = express();

/*app.get('/hello',  (req, res, next) =>{
   res.json('Hello world!');
});*/


app.get('/hello',  (req, res, next) =>{

  res.json(`Response: Hello ${req.query.message}`);
});


app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});